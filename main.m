clc, clear all, close all

%pobranie nazwy pliku
prompt = 'Nazwa pliku: ';
name = input(prompt,'s');

%wczytanie obrazu
I=imread(name);

%wczytanie rozmiarów obrazu
I_height  = size(I,1); 
I_width   = size(I,2);
I_channels = size(I,3);

%Wybór trzech dodatnich współczynników T1<T2<T3 używanych przy kwantyzacji gradientów lokalnych
T1 = 3; 
T2 = 7; 
T3 = 21;

%Macierz bledu dla calego obazu
I_error_corrected=zeros(I_height,I_width,I_channels);

%dla każdego z kanałów
for channel=1:I_channels
	
	%oryginalna macierz
	original=I(:,:,channel);

	%macierz predykcji wstępnej									
	prediction=zeros(I_height,I_width);

	%macierz predykcji po korekcie
	prediction_corrected=zeros(I_height,I_width);

	%macierz błędu wstępnego
	error=zeros(I_height,I_width);

	%macierz błędu po korekcie
	error_corrected=zeros(I_height,I_width);		
	%macierz korekcji
	correction=zeros(I_height,I_width);				

	%macierz 3 gradientów dla danego piksela
	D=zeros(I_height,I_width,3);					

	%macierz 3/wektora/kubełka kontekstów dla danego piksela
	Q=zeros(I_height,I_width,3);

	%macierz znaków kukbełka kontekstów dla danego piksela	
	S=zeros(I_height,I_width);	

	%licznik wystąpień danego kukbełka kontekstów
	N=ones(9,9,9);						

	%skumulowany błąd predykcji dla danego kukbełka kontekstów
	B=zeros(9,9,9); 					

	%aktualna wartość korekcji dla danego kukbełka kontekstów
	C=zeros(9,9,9); 					

	%skumulowany błąd predykcji
	e=0;							


	%Przepisujemy pierwszy wiersz i kolumnę
	prediction(1,:)=original(1,:);
	prediction_corrected(1,:)=original(1,:);
	prediction(:,1)=original(:,1);
	prediction_corrected(:,1)=original(:,1);

	%Obraz przeglądamy w kolejności rastrowej
	for i=2:I_height
		for j=2:I_width
			%===========================================================================
			%Wyznaczamy sąsiednie pixele
			a=original(i,j-1);
			b=original(i-1,j);
			c=original(i-1,j-1);
			if (j == I_width) 
				d=b;
			else d=original(i-1, j+1);
			end;
			%===========================================================================	
			%===========================================================================
			%Dynamiczna predykcja wewnątrzobrazowa(algorytm MED[median edge detector]).
			if c >= max(a,b) 	
				prediction(i,j)=max(a,b);
			elseif c<min(a,b)	
				prediction(i,j)=min(a,b);
			else
	 			prediction(i,j)=a+b-c; 
			end;
			%===========================================================================
			%===========================================================================
			%Wstępny błąd
			error(i,j)=original(i,j)-prediction(i,j);
			%===========================================================================
			%===========================================================================		
			%Wyznaczamy gradienty lokalne
			D(i,j,1)=d-b;
			D(i,j,2)=b-c;
			D(i,j,3)=c-a;
			%if D(i,j,1)==D(i,j,2)==D(i,j,3)==0 runmode
			%============================================================================
			%===========================================================================
			%Kwantyzacja gradientów oraz wyznaczenie składowych wektora kontekstu Q
			for k = 1:3
		        if D(i,j,k)<=-T3			
											Q(i,j,k) = -4;
		        elseif D(i,j,k) <= -T2		
											Q(i,j,k) = -3;
		        elseif D(i,j,k) <= -T1		
											Q(i,j,k) = -2;
		        elseif D(i,j,k) < 0			
											Q(i,j,k) = -1;
		        elseif D(i,j,k) == 0 		
											Q(i,j,k) = 0;
		        elseif (D(i,j,k) < T1)		
											Q(i,j,k) = 1;
		        elseif (D(i,j,k) < T2)		
											Q(i,j,k) = 2;
		        elseif (D(i,j,k) < T3)		
											Q(i,j,k) = 3;
		        else						
											Q(i,j,k) = 4;
				end;
			end;
		
			%===========================================================================
			%Okreslenie znaku na podstawie pierwszej składowej wektora kontekstu
			if Q(i,j,1) < 0     S(i,j) = -1;
			else 				S(i,j) = 1; end
			%===========================================================================
			%===========================================================================
			%Odwzorwanie wektora kontekstu na liczbę

			%n1,n2,n3 - określają numer kubełka kontekstów(musza byc dodatnie)
			n1=S(i,j)*Q(i,j,1)+1; 
			n2=Q(i,j,2)+5; 
			n3=Q(i,j,3)+5;	
		
			B(n1,n2,n3) = B(n1,n2,n3) + e;
			N(n1,n2,n3) = N(n1,n2,n3) + 1;

			if B(n1,n2,n3) <= -N(n1,n2,n3)
				C(n1,n2,n3) = C(n1,n2,n3)-1;
				B(n1,n2,n3) = B(n1,n2,n3)+N(n1,n2,n3);
				if B(n1,n2,n3)<=-N(n1,n2,n3) 	
					B(n1,n2,n3)=-N(n1,n2,n3)+1;end
			elseif B(n1,n2,n3)>0
				C(n1,n2,n3) = C(n1,n2,n3)+1; 
				B(n1,n2,n3) = B(n1,n2,n3)-N(n1,n2,n3);
				if B(n1,n2,n3)>0 
					B(n1,n2,n3)=0;end
			end;

			%algorytm zapominania okresowego
			if N(n1,n2,n3)>127
				N(n1,n2,n3)=64;
				B(n1,n2,n3)=B(n1,n2,n3)/2;end

			e=e+B(n1,n2,n3);		
			correction(i,j)=C(n1,n2,n3);
			%===========================================================================
			%===========================================================================
			%Korekta predykcji
			prediction_corrected(i,j) = prediction(i,j) + S(i,j)*correction(i,j);
			%===========================================================================
			%===========================================================================		
			%Korekta błędu
			error_corrected(i,j) = original(i,j) - prediction_corrected(i,j);
			%===========================================================================
		end;
	end;
	%===========================================================================
	%Wyniki dla pojedynczego kanalu
	
	figure('name', ['Kanal ' num2str(channel)], 'NumberTitle','off');
	hold on;
	
	subplot(2,2,1);
	imshow(original,[0 255]);
	title('Obraz oryginalny');

	subplot(2,2,2);
	imshow(255-error_corrected,[0 255]);
	title('Blad obrazu');

	subplot(2,2,3);
	hist(double(reshape(original,1,[])),300)
	title('Histgram obrazu');

	subplot(2,2,4);
	hist(double(reshape(error_corrected,1,[])),255)
	title('Histogram bledu');
	%===========================================================================
	%===========================================================================
	%zapis bledu
	I_error_corrected(:,:,channel)=error_corrected;
	%===========================================================================
end

%===========================================================================
%wyniki dla calego obrazu
figure('name', name, 'NumberTitle','off')

subplot(2,2,1); 
imshow(I);
title('Obraz oryginalny');

subplot(2,2,2);
imshow(255-I_error_corrected);
title('Blad obrazu');

subplot(2,2,3); 
hist(double(reshape(I,1,[])),300);
title('Histgram obrazu');

subplot(2,2,4);
hist(double(reshape(I_error_corrected,1,[])),255);
title('Histogram bledu');
%===========================================================================
